# Test Dev - Kong
Desarrollar un pequeño sistema para un videoclub, dónde se consulte información de películas, clientes y facturas.

También debe permitir ingresar nuevas facturas a la base de datos a través de un formulario.

Tips:
- No centrarse en maquetado / apartado estético, priorizar lo funcional.
- Nos interesa ver cómo te desenvolvés, trabajá como estás acostumbrado a hacerlo.
- Si no lográs resolver algo, podés entregarlo incompleto. Esto no afecta negativamente a la evaluación que hagamos.

---

## Punto 1
Dadas las tablas `clientes` – `facturas` – `películas`

1. Encontrar Película más vendida.
2. Encontrar cliente que más gasto.

## Punto 2
Hacer un formulario donde se ingrese una factura. 

Los campos son:
- Cliente (guardar idcliente y mostrar nombre - apellido):  tag `<select>`
- Película (guardar idpelicula y mostrar nombre – precio): tag `<select>`
- Botón "Enviar"
	
El formulario debe ser validado con javascript.
Los combos tienen que estar con algún valor valido (valor inicial 0, muestra “seleccione una opción”)
```html
<select value=”0”>Seleccione una opcion</select>
```

Una vez validado el formulario deberá ingresar dichos datos en la base de datos
En la tabla `facturas`.

## Punto 3
Listar clientes por películas en orden alfabético de clientes. El formato debe ser similar al siguiente.

| Cliente 1 | |
| - | - |
| Película 1 | Precio 1 |
| Película 2 | Precio 2 |
| Película n | Precio n |
| | Total |

| Cliente 2 | |
| - | - |
| Película 1 | Precio 1 |
| Película 2 | Precio 2 |
| Película n | Precio n |
| | Total |