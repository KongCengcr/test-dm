<?php 

$dbHost = 'localhost';
$dbName = 'videoclub';
$dbUser = 'root';
$dbPass = '';

try{
    $pdo = new PDO("mysql:host=$dbHost;dbname=$dbName", "$dbUser", "$dbPass");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(Exception $err){
    echo $err->getMessage();
}

?>