<?php 

        require_once 'dbConnection.php';

        /* Consulta para Punto 1 */

        $peliculaMasVendida = $pdo->query("SELECT peliculas.titulo, 
                                            COUNT(peliculas.titulo) as peliculasVendidas, 
                                            peliculas.precio 
                                                FROM facturas 
                                                    JOIN peliculas ON facturas.idpelicula = peliculas.id 
                                                        GROUP BY peliculas.titulo 
                                                            ORDER BY peliculasVendidas DESC LIMIT 1");

        $clienteMasGasto = $pdo->query("SELECT clientes.Nombre, 
                                                    clientes.Apellido, 
                                                    SUM(peliculas.precio) as gastos 
                                                        FROM clientes 
                                                            JOIN facturas ON clientes.idcliente = facturas.idcliente 
                                                            JOIN peliculas ON peliculas.id = facturas.idpelicula 
                                                                GROUP BY clientes.Nombre ORDER BY gastos DESC LIMIT 1");                                                   

        $pelicula = $peliculaMasVendida->fetchAll(PDO::FETCH_ASSOC);

        $cliente = $clienteMasGasto->fetchAll(PDO::FETCH_ASSOC);


        /* Consulta para punto 2 */

        $queryClientes = $pdo->query("SELECT * FROM clientes");
        $clientes = $queryClientes->fetchAll(PDO::FETCH_ASSOC);

        $queryPeliculas = $pdo->query("SELECT * FROM peliculas");
        $peliculas = $queryPeliculas->fetchAll(PDO::FETCH_ASSOC);

        /* Consulta para punto 3 */

            $queryListas = $pdo->query("SELECT clientes.Nombre, 
                                                clientes.Apellido, 
                                                peliculas.titulo,
                                                peliculas.precio,
                                                SUM(peliculas.precio) as totalPrecio
                                                        FROM clientes 
                                                            JOIN facturas ON clientes.idcliente = facturas.idcliente 
                                                            JOIN peliculas ON peliculas.id = facturas.idpelicula 
                                                            GROUP BY clientes.Nombre, peliculas.titulo
                                                                ORDER BY clientes.Nombre ASC");

            $listas = $queryListas->fetchAll(PDO::FETCH_ASSOC);                                                


?>


<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Practica</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.1/css/bootstrap.min.css" integrity="sha384-VCmXjywReHh4PwowAiWNagnWcLhlEJLA5buUprzK8rxFgeH0kww/aWY76TfkUoSX" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <h1>Punto 1</h1>
        <h3>Pelicula más Vendida</h3>
        <table class="table">
            <tr>
                <th>Titulo</th>
                <th>Precio</th>
                <th>Cantidad Vendida</th>
            </tr>
            <tr>
                <td><?php echo $pelicula[0]['titulo']; ?></td>
                <td><?php echo $pelicula[0]['precio']; ?></td>
                <td><?php echo $pelicula[0]['peliculasVendidas']; ?></td>
            </tr>  
        </table>

        <h3>Cliente que más Gato</h3>
        <table class="table">
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Total de Gastos</th>
            </tr>
            <tr>
                <td><?php echo $cliente[0]['Nombre']; ?></td>
                <td><?php echo $cliente[0]['Apellido']; ?></td>
                <td><?php echo $cliente[0]['gastos']; ?></td>
            </tr>  
        </table>

        <br>
        <br>

        <h1>Punto 2</h1>
        <h3>Facturar Pelicula</h3>
        <div class="mt-3" id="aviso">
            
        </div>
        <form id="form" action="facturar.php" method="post" >
            
            <div class="form-group">
                <label for="cliente">Cliente</label>
                <select name="cliente" class="form-control" id="cliente">
                <option value="0"></option>
                <?php
                    foreach($clientes as $cliente) {
                        echo '<option value="'. $cliente['idcliente'] .'">'. $cliente['Nombre'] .' '. $cliente['Apellido'] .'</option>';
                    }
                ?>
                </select>
            </div>

            <div class="form-group">
                <label for="pelicula">Pelicula</label>
                <select name="pelicula" class="form-control" id="pelicula">
                <option value="0"></option>
                <?php
                    foreach($peliculas as $pelicula) {
                        echo '<option value="'. $pelicula['id'] .'">'. $pelicula['titulo']. ' - Precio: '. $pelicula['precio'] .'</option>';
                    }
                ?>
                </select>
            </div>
           
            <input class="btn btn-primary" type="submit" value="Enviar">
        </form>

        <br>
        <br>

        <h1>Punto 3</h1>
        <h3>Facturar Pelicula</h3>
        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Nombre Pelicula</th>
                    <th scope="col">Precio</th>
                </tr>
            </thead>
            <tbody>
            <?php 
            
                $nombreCliente = '';
                $totalPrecio = 0;
                foreach($listas as $lista){
                    
                    if($nombreCliente != $lista['Nombre'] . ' ' . $lista['Apellido']){
                        if($totalPrecio != 0){
                            echo '<tr>';
                            echo    '<td>Total</td>';
                            echo    '<td>'. $totalPrecio .'</td>';
                            echo'</tr>';
                            $totalPrecio = 0;
                        }
                        echo '<tr class="bg-primary">';
                        echo     '<td class="text-white" colspan="2">'.$lista['Nombre'] . ' ' . $lista['Apellido'].'</td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo    '<td>'.$lista['titulo'].'</td>';
                        echo    '<td>'. $lista['precio'] .'</td>';
                        echo'</tr>';
                        $totalPrecio += intval($lista['precio']);
                        $nombreCliente = $lista['Nombre'] . ' ' . $lista['Apellido'];
                    }else{
                        echo '<tr>';
                        echo    '<td>'.$lista['titulo'].'</td>';
                        echo    '<td>'. $lista['precio'] .'</td>';
                        echo'</tr>';
                        $totalPrecio += intval($lista['precio']);
                    }
                }
            
            ?>
            </tbody>
        </table>

    </div>

    <script>
        let formulario = document.querySelector("#form");
        let aviso = document.querySelector("#aviso");

        formulario.addEventListener('submit', (e) => {
            e.preventDefault();

            let datos = new FormData(formulario);

            fetch('facturar.php', {
                method: 'POST',
                body: datos
            })
            .then(res => res.json())
            .then(data => {
                if(data === 'err'){
                    aviso.innerHTML = `<div class="alert alert-danger">
                        Llena Todos los campos
                    </div>`;
                }else{
                    aviso.innerHTML = `<div class="alert alert-success">
                        Factura Procesada
                    </div>`;
                }
            })
        });

    </script>
</body>
</html>